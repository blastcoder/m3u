import sys
import math

import maya.api.OpenMaya as om
import maya.cmds as cmds

maya_useNewAPI = True

# node info
NODE_NAME = "FollowCurve"
NODE_ID = om.MTypeId(0x80003)

COMMAND_NAME = "followCurve"

_author_ = "Philip Melancon"
_version_ = "1.0"

# node class
class FollowCurveNode(om.MPxNode) :

    # constructor
    def __init__(self) :

        super(FollowCurveNode, self).__init__()

    @staticmethod
    def cmdCreator():
        '''Create an instance of our node. '''
        return FollowCurveNode()

    def compute(self, plug, data):
        """Method that constructs a global transformation matrix from the input values and outputs a modified local matrix."""

        # GET THE ROTATION ORDER OBJECT
        inRotationOrderHandle = data.inputValue(self.rotationOrder) # Get the rotation order handle
        rotOrder = inRotationOrderHandle.asInt() # Get the rotation order object from the handle

        # GET THE NURBS CURVE
        curve = om.MFnNurbsCurve(data.inputValue(self.inputCurve).asNurbsCurve()) # Get Curve object

        # GET THE WORLD TRANSFORMS OF THE CONTROL
        ctlGlobalMtx = self.getControlGlobalMtx(data) # Compute the control's global matrix based on the input data
        transformDict = self.deconstructTransformMtx(ctlGlobalMtx, rotOrder) # Extract the translate and rotate parameters from the matrix

        # Get the transform to be used as world offset
        offsetMtx = self.getTransformFromNurbs(curve, data, transformDict)

        # Apply the local transforms to the offset matrix to get the global transform
        ctlGlobalMtx = self.applyTransformsToMatrix(transformDict, rotOrder, offsetMtx)

        # Convert the global transform matrix to the control's local coordinate system
        ctlLocalMtx = om.MTransformationMatrix(ctlGlobalMtx.asMatrix() * data.inputValue(self.controlParentMatrix).asMatrix().inverse())

        # De-construct control local matrix
        outTransformDict = self.deconstructTransformMtx(ctlLocalMtx, rotOrder)

        # Get the new local position from the transform dictionary
        newPos = outTransformDict["position"]

        # Get the new local rotation from the transform dictionary
        newRot = outTransformDict["rotation"]

        # Get the output position handle
        outPositionHandle = data.outputValue(self.outputPosition)

        # Get the output rotation handle
        outRotationHandle = data.outputValue(self.outputRotation)

        # Port the values to the ouput
        outPositionHandle.set3Float(newPos[0], newPos[1], newPos[2])
        outRotationHandle.set3Float(newRot[0], newRot[1], newRot[2])

    def applyTransformsToMatrix(self, transformDict, rotOrder, mTransformMtx):
        """Apply transforms from input to the matrix."""

        # Get the position list from the dictionary
        pos = transformDict["position"]

        # Create a vector from the x and y components of the original world position
        offsetPoint = om.MFloatPoint(pos[0], pos[1], 0.0)
        offsetVec = om.MVector(offsetPoint)

        # Move the offset matrix by the offset vector
        mTransformMtx.translateBy(offsetVec, om.MSpace.kPreTransform)

        # Get the rotation euler angles in radians
        rot = [math.radians(i) for i in transformDict["rotation"]]

        # Create a MEulerRotation object from the rotation and rotation order
        mEuler = om.MEulerRotation(rot[0], rot[1], rot[2], rotOrder)

        # Rotate the offset matrix
        mTransformMtx.rotateBy(mEuler, om.MSpace.kPreTransform)

        return mTransformMtx

    def getTransformFromNurbs(self, curve, data, transformDict):
        """Method that computes a transform to be used as world offset for the control based on the input curve."""

        # Find the U value matching the Z distance
        outU = curve.findParamFromLength(transformDict["position"][2])

        # Find the coordinates of that value along the curve
        pVec = om.MVector(curve.getPointAtParam(outU, om.MSpace.kWorld))
        # Find the tangent of the curve for that point
        tan = curve.tangent(outU, om.MSpace.kWorld)
        # If the Z position is outside of the range of the curve, extrapolate the position based on the tangent of the start/end point of the curve
        if transformDict["position"][2] < 0:
            pVec += om.MVector(tan.normalize())*transformDict["position"][2]
        elif transformDict["position"][2] > curve.length():
            pVec += om.MVector(tan.normalize())*(transformDict["position"][2] - curve.length())

        # Collapse Y component of the tangent so force a flat ground and avoid messy normals
        tan.y = 0
        tan.normalize()
        zVec = om.MVector(tan)
        yVec = om.MVector(0.0, 1.0, 0.0)

        # Calculate the cross product of the z and y vectors to get the x axis
        xVec = yVec^zVec

        # Create the local offset matrix
        localOffsetMtx = om.MMatrix([xVec.x, xVec.y, xVec.z]+[0]+[yVec.x, yVec.y, yVec.z]+[0]+[zVec.x, zVec.y, zVec.z]+[0]+[pVec.x, pVec.y, pVec.z]+[1])

        # Multiply the local offset matrix with the curve's global transform in case the curve is moved around or rotated
        globalOffsetMtx = om.MTransformationMatrix(localOffsetMtx * data.inputValue(self.curveWorldMatrix).asMatrix())

        return globalOffsetMtx


    def getControlGlobalMtx(self, data):
        """Method that extracts and formats the input data from the control."""

        posMVector = om.MVector(data.inputValue(self.position).asFloat3())
        rot = data.inputValue(self.rotation).asFloat3()
        rotOrder = data.inputValue(self.rotationOrder).asInt()
        rotation = [math.radians(i) for i in rot]

        # Create a euler rotation
        mEuler = om.MEulerRotation(rotation[0], rotation[1], rotation[2], rotOrder)

        mTransformMtx = om.MTransformationMatrix()

        # Set the transform matrix position in world space
        mTransformMtx.setTranslation(posMVector, om.MSpace.kWorld)

        # Set the transform matrix rotation
        #mTransformMtx.rotateTo(mEuler)
        mTransformMtx.setRotation(mEuler)

        parMtx = data.inputValue(self.controlParentMatrix).asMatrix()
        childGlobalMtx = om.MTransformationMatrix(mTransformMtx.asMatrix() * parMtx)

        return childGlobalMtx

    def deconstructTransformMtx(self, mTransformMtx, rotationOrder=0):
        """Method that de-constructs a MTransformationMatrix object.
        Returns a dictionary with "position" and "rotation" keys that point to lists of 3 floats.
        Rotation is represented in degrees while position is in maya units."""

        # Extract the position from the transform matrix
        pos = mTransformMtx.translation(om.MSpace.kWorld)

        # Extract the Euler rotation from the transform matrix
        rot = mTransformMtx.rotation(asQuaternion=False)

        # Reorder the Euler values based on the rotation order
        rot.reorderIt(rotationOrder)

        # Convert the radian angles to degrees
        rot = [math.degrees(angle) for angle in (rot.x, rot.y, rot.z)]

        # Return the dictionary
        return {"position": [pos.x, pos.y, pos.z], "rotation": rot}


class FollowCurveCommand(om.MPxCommand):

    def __init__(self):
        ''' Constructor. '''
        super(FollowCurveCommand, self).__init__()
        self.objects = []
        self.nodes = []

    @staticmethod
    def cmdCreator():
        ''' Create an instance of our command. '''
        return FollowCurveCommand()

    def doIt(self, args):
        ''' Command execution. '''

        objects = self.parseArguments(args)
        if not objects:
            objects = self.parseSelection(om.MGlobal.getActiveSelectionList())

        if len(objects) < 2:
            raise Exception("This command requires at least 2 objects")

        path = objects[0]
        controls = objects[1:]

        for ctrl in controls:
            self.setToPath(path, ctrl)

        self.setResult(self.nodes)

    def setToPath(self, path, ctrl):
        '''Links the control to the curve'''

        # CREATE FOLLOW CURVE NODE AND CONNECT ITS MAIN ATTRIBUTES
        node = cmds.createNode("FollowCurve")
        cmds.connectAttr("%s.local" % cmds.listRelatives(path, ad=True, s=True)[0], "%s.inputCurve" % node, force=True)
        cmds.connectAttr("%s.worldMatrix[0]" % path, "%s.curveWorldMatrix" % node, force=True)
        cmds.connectAttr("%s.parentMatrix[0]" % ctrl, "%s.controlParentMatrix" % node, force=True)
        cmds.connectAttr("%s.rotateOrder" % ctrl, "%s.rotationOrder" % node, force=True)

        blend = cmds.pairBlend(node=ctrl, at=['tx','ty', 'tz', 'rx', 'ry', 'rz'])

        # CONNECT CTRL FCURVES TO THE FOLLOWCURVE NODE
        channels = ["translate", "rotate"]
        axis = "XYZ"
        for channel in channels:
            for a in axis:
                src = "%s.in%s%s1" % (blend, channel.title(), a)
                dst = "%s.%s%s" % (node, channel.replace("translate", "position").replace("rotate", "rotation"), a)
                fcurve = cmds.listConnections(src, type="animCurve")
                if fcurve:
                    cmds.connectAttr("%s.output" % fcurve[0], dst, force=True)
                else:
                    cmds.setAttr(dst, cmds.getAttr(src))

        for a in axis:
            try:
                cmds.connectAttr("%s.outputPosition%s" % (node, a), "%s.inTranslate%s2" % (blend, a), f=True)
            except:
                pass
            try:
                cmds.connectAttr("%s.outputRotation%s" % (node, a), "%s.inRotate%s2" % (blend, a), f=True)
            except:
                pass

        self.nodes.append(node)


    def parseArguments(self, args, index=0):
        ''' Parses and validates arguments '''
        try:
            value = args.asString(index)
            if cmds.objExists(value):
                self.objects.append(value)
            self.parseArguments(args, index+1)
        except:
            pass

        return self.objects

    def parseSelection(self, args, index=0):
        try:
            value = args.getSelectionStrings(index)[0]
            if cmds.objExists(value):
                self.objects.append(value)
            self.parseSelection(args, index+1)
        except:
            pass

        return self.objects


class RemoveFollowCurveCommand(om.MPxCommand):

    def __init__(self):
        super(RemoveFollowCurveCommand, self).__init__()
        self.objects = []

    @staticmethod
    def cmdCreator():

        return RemoveFollowCurveCommand()

    def doIt(self, args):
        objects = self.parseArguments(args)
        if not objects:
            objects = self.parseSelection(om.MGlobal.getActiveSelectionList())

        self.disconnectNodes(objects)

    def parseArguments(self, args, index=0):
        ''' Parses and validates arguments '''
        try:
            value = args.asString(index)
            if cmds.objExists(value):
                self.objects.append(value)
            self.parseArguments(args, index+1)
        except:
            pass

        return self.objects

    def parseSelection(self, args, index=0):
        try:
            value = args.getSelectionStrings(index)[0]
            if cmds.objExists(value):
                self.objects.append(value)
            self.parseSelection(args, index+1)
        except:
            pass

        return self.objects

    def disconnectNodes(self, objects):
        nodes = []
        for i in objects:
            connections = cmds.listConnections(i, type="FollowCurve")
            if not connections:
                connections = cmds.listConnections(cmds.listRelatives(i, ad=True, s=True)[0], type="FollowCurve")
            nodes.extend(connections)
        cmds.delete(list(set(nodes)))


# node initializer; sets up the available inputs and outputs
def nodeInitializer() :
    numAttr = om.MFnNumericAttribute()
    dropAttr = om.MFnEnumAttribute()
    matAttr = om.MFnMatrixAttribute()
    typedAttr = om.MFnTypedAttribute()

    # Setup the input attributes
    # Input position
    FollowCurveNode.position = numAttr.createPoint("position", "pos")

    # Input rotation
    FollowCurveNode.rotation = numAttr.createPoint("rotation", "rot")

    # Input rotation order
    FollowCurveNode.rotationOrder = dropAttr.create("rotationOrder", "rotOrder")
    dropAttr.addField("XYZ", 0)
    dropAttr.addField("YZX", 1)
    dropAttr.addField("ZXY", 2)
    dropAttr.addField("XZY", 3)
    dropAttr.addField("YXZ", 4)
    dropAttr.addField("ZYX", 5)

    # Offset matrix
    FollowCurveNode.controlParentMatrix = matAttr.create("controlParentMatrix", "cpm")

    # Input nurbs curve
    FollowCurveNode.inputCurve = typedAttr.create("inputCurve", "inCrv", om.MFnData.kNurbsCurve)
    FollowCurveNode.curveWorldMatrix = matAttr.create("curveWorldMatrix", "cwm")

    # Setup the output attributes

    FollowCurveNode.outputPosition = numAttr.createPoint("outputPosition", "outPos")

    FollowCurveNode.outputRotation = numAttr.createPoint("outputRotation", "outRot")

    # add the attributes to the node
    FollowCurveNode.addAttribute(FollowCurveNode.position)
    FollowCurveNode.addAttribute(FollowCurveNode.rotation)
    FollowCurveNode.addAttribute(FollowCurveNode.rotationOrder)
    FollowCurveNode.addAttribute(FollowCurveNode.controlParentMatrix)
    FollowCurveNode.addAttribute(FollowCurveNode.inputCurve)
    FollowCurveNode.addAttribute(FollowCurveNode.curveWorldMatrix)
    FollowCurveNode.addAttribute(FollowCurveNode.outputPosition)
    FollowCurveNode.addAttribute(FollowCurveNode.outputRotation)

    # Set the attribute dependencies
    FollowCurveNode.attributeAffects(FollowCurveNode.controlParentMatrix, FollowCurveNode.outputPosition)
    FollowCurveNode.attributeAffects(FollowCurveNode.position, FollowCurveNode.outputPosition)
    FollowCurveNode.attributeAffects(FollowCurveNode.inputCurve, FollowCurveNode.outputPosition)
    FollowCurveNode.attributeAffects(FollowCurveNode.curveWorldMatrix, FollowCurveNode.outputPosition)
    FollowCurveNode.attributeAffects(FollowCurveNode.position, FollowCurveNode.outputRotation)
    FollowCurveNode.attributeAffects(FollowCurveNode.rotation, FollowCurveNode.outputRotation)
    FollowCurveNode.attributeAffects(FollowCurveNode.rotationOrder, FollowCurveNode.outputRotation)
    FollowCurveNode.attributeAffects(FollowCurveNode.inputCurve, FollowCurveNode.outputRotation)
    FollowCurveNode.attributeAffects(FollowCurveNode.controlParentMatrix, FollowCurveNode.outputRotation)
    FollowCurveNode.attributeAffects(FollowCurveNode.curveWorldMatrix, FollowCurveNode.outputRotation)


# initialize the plug-in
def initializePlugin(mobject) :
    mplugin = om.MFnPlugin(mobject)
    mplugin.version = _version_
    try:
        mplugin.registerNode(NODE_NAME, NODE_ID, FollowCurveNode.cmdCreator, nodeInitializer)
    except:
        sys.stderr.write("Failed to register node: %s\n" % NODE_NAME)
    try:
        mplugin.registerCommand(COMMAND_NAME, FollowCurveCommand.cmdCreator)
    except:
        sys.stderr.write("Failed to register command: %s\n" % COMMAND_NAME)
    try:
        mplugin.registerCommand("removeFollowCurve", RemoveFollowCurveCommand.cmdCreator)
    except:
        sys.stderr.write("Failed to register command: removeFollowCurve\n")

# uninitialize the plug-in
def uninitializePlugin(mobject):
    mplugin = om.MFnPlugin(mobject)
    try:
        mplugin.deregisterNode(NODE_ID)
    except:
        sys.stderr.write("Failed to unregister node: %s\n" % NODE_NAME)
    try:
        mplugin.deregisterCommand(COMMAND_NAME)
    except:
        sys.stderr.write("Failed to unregister command: %s\n" % COMMAND_NAME)
    try:
        mplugin.deregisterCommand("removeFollowCurve")
    except:
        sys.stderr.write("Failed to unregister command: removeFollowCurve\n")
